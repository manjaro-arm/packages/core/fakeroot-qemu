# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor:  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Allan McRae <allan@archlinux.org>
# Contributor: Jochem Kossen <j.kossen@home.nl>

pkgname=fakeroot-qemu
_pkgname=fakeroot
pkgver=1.28
pkgrel=1
pkgdesc='Tool for simulating superuser privileges, configured to work with qemu'
arch=('x86_64' 'aarch64')
license=(GPL)
url='https://packages.debian.org/fakeroot'
install=fakeroot.install
depends=(glibc filesystem sed util-linux sh)
makedepends=(po4a)
provides=('fakeroot')
conflicts=('fakeroot')
source=("https://deb.debian.org/debian/pool/main/f/$_pkgname/${_pkgname}_${pkgver}.orig.tar.gz")
sha256sums=('56d405e36ff685f83879be08fdd654255ab9aa38632b4605a98e896ad63990c2')

prepare() {
  cd $_pkgname-$pkgver

  ./bootstrap
}

build() {
  cd $_pkgname-$pkgver

  ./configure --prefix=/usr \
    --libdir=/usr/lib/libfakeroot \
    --disable-static \
    --with-ipc=tcp

  make

  cd doc
  po4a -k 0 --rm-backups --variable 'srcdir=../doc/' po4a/po4a.cfg
}

package() {
  cd $_pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  install -dm755 "$pkgdir/etc/ld.so.conf.d/"
  echo '/usr/lib/libfakeroot' > "$pkgdir/etc/ld.so.conf.d/fakeroot.conf"

  # install README for sysv/tcp usage
  install -Dm644 README "$pkgdir/usr/share/doc/$_pkgname/README"
}
